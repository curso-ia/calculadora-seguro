package br.org.senai.ia;

public class Perfil {

    private SexoEnum sexo;

    private Integer idade;

    private EstadoCivilEnum estadoCivil;

    private Integer filhos;

    private Boolean consumoAlcoolico;

    private Boolean usaVeiculoParaTrabalho;

    private Integer kilometragemPorMes;

    private Integer tempoCNH;

    public SexoEnum getSexo() {
        return sexo;
    }

    public void setSexo(SexoEnum sexo) {
        this.sexo = sexo;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public EstadoCivilEnum getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Integer getFilhos() {
        return filhos;
    }

    public void setFilhos(Integer filhos) {
        this.filhos = filhos;
    }

    public Boolean getConsumoAlcoolico() {
        return consumoAlcoolico;
    }

    public void setConsumoAlcoolico(Boolean consumoAlcoolico) {
        this.consumoAlcoolico = consumoAlcoolico;
    }

    public Boolean getUsaVeiculoParaTrabalho() {
        return usaVeiculoParaTrabalho;
    }

    public void setUsaVeiculoParaTrabalho(Boolean usaVeiculoParaTrabalho) {
        this.usaVeiculoParaTrabalho = usaVeiculoParaTrabalho;
    }

    public Integer getKilometragemPorMes() {
        return kilometragemPorMes;
    }

    public void setKilometragemPorMes(Integer kilometragemPorMes) {
        this.kilometragemPorMes = kilometragemPorMes;
    }

    public Integer getTempoCNH() {
        return tempoCNH;
    }

    public void setTempoCNH(Integer tempoCNH) {
        this.tempoCNH = tempoCNH;
    }


}
