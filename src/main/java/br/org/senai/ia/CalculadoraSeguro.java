package br.org.senai.ia;

import java.math.BigDecimal;

public class CalculadoraSeguro {

    public BigDecimal calcula(BigDecimal valorBase, Perfil perfil) {

        Integer percentual = 0;

        percentual = calculaPorSexo(perfil.getSexo(), percentual);
        percentual = calculaPorIdade(perfil.getIdade(), percentual);
        percentual = calculaPorEstadoCivil(perfil.getEstadoCivil(), percentual);
        percentual = calculaPorFilhosNaoCondutores(perfil.getFilhos(), percentual);
        percentual = calculaPorConsumoAlcoolico(perfil.getConsumoAlcoolico(), percentual);
        percentual = calculaPorUsoParaTrabalho(perfil.getUsaVeiculoParaTrabalho(), percentual);
        percentual = calculaPorKMMensal(perfil.getKilometragemPorMes(), percentual);
        percentual = calculaPorTempoCNH(perfil.getTempoCNH(), percentual);

        BigDecimal percentualDecimal = BigDecimal.valueOf(percentual).divide(BigDecimal.valueOf(100)).setScale(2);
        return valorBase.add(valorBase.multiply(percentualDecimal));
    }

    private Integer calculaPorTempoCNH(Integer tempoCNH, Integer percentual) {
        Integer result = tempoCNH / 5;
        return result > 0 ? percentual + (result * -1) : percentual;
    }

    Integer calculaPorKMMensal(Integer kmMensal, Integer percentual) {
        Integer result = kmMensal / 1000;
        return result > 0 ? percentual + result : percentual;
    }

    private Integer calculaPorUsoParaTrabalho(Boolean usaParaTrabalho, Integer percentual) {
        return usaParaTrabalho ? percentual + 2 : percentual;
    }

    private Integer calculaPorConsumoAlcoolico(Boolean comsumoAlcoolico, Integer percentual) {
        return comsumoAlcoolico ? percentual + 10 : percentual;
    }

    private Integer calculaPorFilhosNaoCondutores(Integer quantidadeFilhos, Integer percentual) {
        return quantidadeFilhos > 0 ? percentual + (quantidadeFilhos * -1) : percentual;
    }

    private int calculaPorEstadoCivil(EstadoCivilEnum estadoCivil, Integer percentual) {
        return EstadoCivilEnum.CASADO.equals(estadoCivil) ? percentual - 5 : percentual + 15;
    }

    private Integer calculaPorIdade(Integer idade, Integer percentual) {
        if (idade <= 21) {
            percentual += 10;
        } else if (idade <= 25) {
            percentual += 8;
        } else if (idade <= 30) {
            percentual += 6;
        } else if (idade > 50) {
            int result = idade / 5;
            percentual += (result - 10);
        }
        return percentual;
    }

    private int calculaPorSexo(SexoEnum sexo, Integer percentual) {
        return SexoEnum.M.equals(sexo) ? percentual + 7 : percentual + 3;
    }
}
