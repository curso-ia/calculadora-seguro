package br.org.senai.ia;

public enum EstadoCivilEnum {
    SOLTEIRO, CASADO;
}
