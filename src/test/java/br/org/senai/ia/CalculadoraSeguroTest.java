package br.org.senai.ia;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class CalculadoraSeguroTest {

    private CalculadoraSeguro calculadoraSeguro;

    private Perfil perfil;

    @Before
    public void setUp() {
        calculadoraSeguro = new CalculadoraSeguro();

        perfil = new Perfil();
        perfil.setConsumoAlcoolico(false);
        perfil.setEstadoCivil(EstadoCivilEnum.CASADO);
        perfil.setFilhos(1);
        perfil.setIdade(34);
        perfil.setKilometragemPorMes(1300);
        perfil.setSexo(SexoEnum.M);
        perfil.setTempoCNH(16);
        perfil.setUsaVeiculoParaTrabalho(false);
    }

    @Test
    public void calcula() {
        BigDecimal valorTotal = calculadoraSeguro.calcula(BigDecimal.valueOf(700), perfil);
        System.out.println(valorTotal);
        assertEquals(BigDecimal.valueOf(693.00).setScale(2), valorTotal);
    }

    @Test
    public void calculaPorKMMensal_1() {
        Integer percentual = calculadoraSeguro.calculaPorKMMensal(1050, 0);
        assertSame(1, percentual);
    }

    @Test
    public void calculaPorKMMensal_1_comSoma() {
        Integer percentual = calculadoraSeguro.calculaPorKMMensal(1050, 10);
        assertSame(11, percentual);
    }

    @Test
    public void calculaPorKMMensal_0() {
        Integer percentual = calculadoraSeguro.calculaPorKMMensal(999, 0);
        assertSame(0, percentual);
    }

    @Test
    public void calculaPorKMMensal_0_comSoma() {
        Integer percentual = calculadoraSeguro.calculaPorKMMensal(999, 15);
        assertSame(15, percentual);
    }
}
